using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class Bot
{
    /// <summary>
    /// Application Main
    /// </summary>
    /// <param name="args"></param>
    public static void Main(string[] args)
    {
        string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];

        Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        using (TcpClient client = new TcpClient(host, port))
        {
            NetworkStream stream = client.GetStream();
            StreamReader reader = new StreamReader(stream);
            StreamWriter writer = new StreamWriter(stream);
            writer.AutoFlush = true;

            // Create Bot Object
            new Bot(reader, writer, new Join(botName, botKey));
        }
    }

    private StreamWriter writer;

    private Bot(StreamReader reader, StreamWriter writer, Join join)
    {
        this.writer = writer;
        string line;

        TrackPieces trackPieces = null;

        // Sends sonnection request
        Send(join);

        while ((line = reader.ReadLine()) != null)
        {
            MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
            switch (msg.msgType)
            {
                case "carPositions":
                    CarPosition carPosition = new CarPosition(line);

                    double throttle = 0.1;

                    if (trackPieces != null)
                    {
                        trackPieces.SpeedPerTrackPiece.TryGetValue(carPosition.TrackPiece, out throttle);
                    }

                    if (carPosition.CarDrfitAngle > 20)
                    {
                        Send(new Throttle(0.3));
                    }
                    else if (carPosition.CarDrfitAngle > 10)
                    {
                        Send(new Throttle(0.5));
                    }
                    else if (carPosition.CarDrfitAngle > 2)
                    {
                        Send(new Throttle(Math.Min(0.7, throttle)));
                    }
                    else
                    {
                        Send(new Throttle(throttle));
                    }

                    break;
                case "join":
                    Console.WriteLine("Joined");
                    Send(new Ping());
                    break;
                case "gameInit":
                    Console.WriteLine("Race init");

                    // Save Track Info
                    trackPieces = new TrackPieces(line);

                    // Send Ping to Server
                    Send(new Ping());
                    break;
                case "gameEnd":
                    Console.WriteLine("Race ended");
                    Send(new Ping());
                    break;
                case "gameStart":
                    Console.WriteLine("Race starts");
                    Send(new Ping());
                    break;
                default:
                    Send(new Ping());
                    break;
            }
        }
    }

    private void Send(SendMsg msg)
    {
        writer.WriteLine(msg.ToJson());
    }
}

internal class MsgWrapper
{
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, Object data)
    {
        this.msgType = msgType;
        this.data = data;
    }
}

/// <summary>
/// Parses JSON Object to extract required Car Position Info
/// </summary>
internal class CarPosition
{
    public string name;
    public string angle;
    public int CarDrfitAngle = 0;
    public string TrackPiece = "0";

    public CarPosition(string jsonObject)
    {
        var jo = JObject.Parse(jsonObject);
        JArray data = (JArray)jo["data"];

        foreach (var info in data)
        {
            var name = (info.SelectToken("id")).SelectToken("name");
            if (name.ToString().Equals("Excelencia"))
            {
                angle = info.SelectToken("angle").ToString();
                TrackPiece = (info.SelectToken("piecePosition")).SelectToken("pieceIndex").ToString();
                break;
            }
        }

        int.TryParse(angle, out CarDrfitAngle);
    }
}

/// <summary>
/// Parses JSON Object for Track Pieces and holds the information information
/// </summary>
internal class TrackPieces
{
    private int _index = 0;

    /// <summary>
    /// Contains Throttle speed for each piece in the track
    /// Key = Piece Index
    /// Value = Throttle Value
    /// </summary>
    public Dictionary<string, double> SpeedPerTrackPiece = new Dictionary<string, double>();

    /// <summary>
    /// Argument Constructor
    /// </summary>
    /// <param name="jsonString"></param>
    public TrackPieces(string jsonString)
    {
        var jo = JObject.Parse(jsonString);
        JArray data = (JArray)jo["data"]["race"]["track"]["pieces"];

        foreach (var info in data)
        {
            var angle = info.SelectToken("angle");
            double throttle = 0.9;
            if (angle != null)
            {
                throttle = Math.Abs(Convert.ToDecimal(angle)) > 40 ? 0.3 : 0.5;
            }

            SpeedPerTrackPiece.Add((++_index).ToString(),throttle);
        }
    }
}

internal abstract class SendMsg
{
    public string ToJson()
    {
        return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
    }

    protected virtual Object MsgData()
    {
        return this;
    }

    protected abstract string MsgType();
}

/// <summary>
/// Contains Connection information
/// </summary>
internal class Join : SendMsg
{
    public string name;
    public string key;
    public string color;

    public Join(string name, string key)
    {
        this.name = name;
        this.key = key;
        this.color = "red";
    }

    protected override string MsgType()
    {
        return "join";
    }
}

//internal class CreateRace : SendMsg
//{
//    public string name;
//    public string key;
//    public string trackName;
//    public string password;
//    public string carCount;

//    public CreateRace(string name, string key)
//    {
//        this.name = name;
//        this.key = key;
//    }


//    protected override string MsgType()
//    {
//        return "createRace";
//    }
//}


internal class Ping : SendMsg
{
    protected override string MsgType()
    {
        return "ping";
    }
}

internal class Throttle : SendMsg
{
    public double value;

    public Throttle(double value)
    {
        this.value = value;
    }

    protected override Object MsgData()
    {
        return this.value;
    }

    protected override string MsgType()
    {
        return "throttle";
    }
}